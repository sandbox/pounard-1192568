<?php

/**
 * @file
 * Configuration API.
 */

/**
 * Base exception for the configuration API.
 */
class DrupalConfigException extends Exception {}

/**
 * Configuration objects interface.
 */
interface DrupalConfigInterface {
  /**
   * Set parent for chaining.
   * 
   * @param DrupalConfigInterface $parent
   * 
   * @return DrupalConfigInterface
   *   Self reference for chaining.
   */
  public function setParent(DrupalConfigInterface $parent);

  /**
   * Get value at given path.
   * 
   * @param string $path
   * 
   * @return scalar
   */
  public function get($path);

  /**
   * Set value at given path.
   * 
   * @param string $path
   * @param scalar $value
   * 
   * @return DrupalConfigInterface
   *   Self reference for chaining.
   */
  public function set($path, $value);

  /**
   * Remove value at given path.
   * 
   * @param string $path
   * 
   * @return DrupalConfigInterface
   *   Self reference for chaining.
   */
  public function del($path);

  /**
   * Get default value at given path.
   * 
   * @param string $path
   * 
   * @return scalar
   */
  public function def($path);
}

/**
 * Array implementation of config objects. 
 * 
 * This non cached, extremely simple implementation will allow you to create
 * arbitrary configuration objects for simple needs or global contextual
 * overrides.
 */
class DrupalConfigArray implements DrupalConfigInterface {
  /**
   * @var DrupalConfigInterface
   */
  protected $_parent;

  public function setParent(DrupalConfigInterface $parent) {
    $this->_parent = $parent;
    return $this;
  }

  /**
   * @var array
   */
  protected $_values;

  public function get($path) {
    if (array_key_exists($path, $this->_values)) {
      return $this->_values[$path];
    }
    else if (isset($this->_parent)) {
      return $this->_parent->get($path);
    }
  }

  public function set($path, $value) {
    $this->_values[$path] = $value;
    return $this;
  }

  public function del($path) {
    unset($this->_values[$path]);
    return $this;
  }

  public function def($path) {
    return NULL;
  }

  public function __construct($values = array()) {
    $this->_values = $values;
  }
}

/**
 * Null object pattern implementation for DrupalConfigInterface.
 */
class DrupalConfigNull implements DrupalConfigInterface {
  /**
   * @var DrupalConfigNull
   */
  private static $__instance;

  /**
   * Null object pattern and flyweight pattern implementation. share the same
   * null instance whenever possible.
   */
  public static function instance() {
    if (!isset(self::$__instance)) {
      self::$__instance = new self;
    }
    return self::$__instance;
  }

  /**
   * Singleton pattern implementation.
   */
  private function __construct() {}

  public function setParent(DrupalConfigInterface $parent) {}

  public function get($path) {
    return NULL;
  }

  public function set($path, $value) {
    return $this;
  }

  public function del($path) {
    return $this;
  }

  public function def($path) {
    return NULL;
  }
}

/**
 * Instance that will be able to pop the associated context from the stack when
 * it goes out of scope.
 */
class DrupalConfigStackToken {
  /**
   * @var string
   */
  protected $_token;

  /**
   * Get token.
   * 
   * @return string
   */
  public function getToken() {
    return $this->_token;
  }

  /**
   * Release associated item from the stack.
   */
  public function release() {
    DrupalConfigStack::unstack($this->_token);
  }

  /**
   * Destructor implementation.
   * FIXME: This one is dangerous.
   */
  public function __destruct() {
    $this->release();
  }

  /**
   * Default constructor.
   * 
   * @param string $token
   */
  public function __construct($token) {
    $this->_token = $token;
  }
}

/**
 * Static configuration manager.
 */
class DrupalConfigStack {
  /**
   * Configuration objects stack.
   * 
   * @var array
   */
  protected static $_stack = array();

  /**
   * Get current full stack as an array.
   * 
   * @return array
   *   Keyed array of DrupalConfigInterface instances.
   */
  public static function getStack() {
    return self::$_stack;
  }

  /**
   * Stack new configuration instance, that will be put on top of the stack.
   * 
   * The new configuration object will override all existing ones.
   * 
   * @param DrupalConfigInterface $config
   * @param string $name = NULL
   *   If set, the current configuration object will be stored permanently
   *   instead of being volatile. If you set a name here, the token won't
   *   be created.
   * 
   * @return DrupalConfigStackToken
   *   Or NULL if configuration object is named.
   */
  public static function stack(DrupalConfigInterface $config, $name = NULL) {
    if (empty(self::$_stack)) {
      $config->setParent(DrupalConfigNull::instance());
    }
    else {
      end(self::$_stack);
      $config->setParent(current(self::$_stack));
    }

    if (isset($name)) {
      self::$_stack[$name] = $config;
    }
    else {
      $token = spl_object_hash($config);
      // Using strings for array key won't alter push order.
      self::$_stack[$token] = $config;
      return new DrupalConfigStackToken($token);
    }
  }

  /**
   * Remove given configuration element from stack.
   * 
   * @param string $token
   */
  public static function unstack($token) {
    if ($token instanceof DrupalConfigStackToken) {
      unset(self::$_stack[$token->getToken()]);
    }
    else {
      unset(self::$_stack[$token]);
    }
  }

  /**
   * Get current instance.
   * 
   * @return DrupalConfigInterface
   */
  public static function current() {
    if (empty(self::$_stack)) {
      return DrupalConfigNull::instance();
    }

    end(self::$_stack);
    return current(self::$_stack);
  }

  /**
   * Get named or tokened instance.
   * 
   * @param string $key
   *   Name or string token value.
   * 
   * @return DrupalConfigInterface
   */
  public static function getByKey($key = NULL) {
    return isset(self::$_stack[$key]) ? self::$_stack[$key] : DrupalConfigNull::instance();
  }
}

/**
 * Get a contextual configuration value.
 * 
 * // FIXME: Better end user documentation.
 * 
 * @param string $path
 * @param scalar $default = NULL
 *   Set here a default value for early bootstrap phases. For other usages, don't use it.
 * 
 * @return scalar
 */
function config_get($path, $default = NULL) {
  $value = DrupalConfigStack::current()->get($path);
  return isset($value) ? $value : $default;
}

/**
 * Set a system configuration value.
 * 
 * // FIXME: Better end user documentation.
 * 
 * @param string $path
 * @param scalar $value
 * @param string $target = 'system'
 */
function config_set($path, $value, $target = 'system') {
  DrupalConfigStack::getByKey($target)->set($path, $value);
}

/**
 * Delete a system configuration value.
 * 
 * // FIXME: Better end user documentation.
 * 
 * @param string $path
 * @param string $target = 'system'
 */
function config_del($path, $target = 'system') {
  DrupalConfigStack::getByKey($target)->del($path);
}
