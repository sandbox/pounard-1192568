<?php

/**
 * @file
 * Configuration API system specifics.
 */

/**
 * Config cache identifier.
 */
define('CONFIG_SYSTEM_CID', 'config_system_cache');

/**
 * Configuration related static helpers container.
 */
class DrupalConfigSystemHelper {
  /**
   * @var array
   */
  protected static $_schemaCache;

  /**
   * Get schema definition for given path.
   * 
   * @param string $path
   * 
   * @return array
   *   Key schema, or NULL if not found.
   */
  public static function getSchema($path) {
    // FIXME: Better implementation to come. Schema could also be stored into
    // external files. This could also be cached.
    if (!isset(self::$_schemaCache)) {
      self::$_schemaCache = module_invoke_all('config_schema');
    }
    return isset(self::$_schemaCache[$path]) ? self::$_schemaCache[$path] : NULL;
  }
}

/**
 * System configuration.
 * 
 * FIXME: Handle caching and default values.
 */
class DrupalConfigSystem implements DrupalConfigInterface {
  /**
   * @var DrupalConfigInterface
   */
  protected $_parent;

  public function setParent(DrupalConfigInterface $parent) {
    $this->_parent = $parent;
    return $this;
  }

  /**
   * @var array
   */
  protected $_cache;

  public function get($path) {
    return array_key_exists($path, $this->_cache) ? $this->_cache[$path] : $this->_parent->get($path);
  }

  public function set($path, $value) {
    $this->_cache[$path] = $value;
  }

  public function del($path) {
    unset($this->_cache[$path]);
  }

  public function def($path) {
    // FIXME: Todo.
    return NULL;
  }

  /**
   * Fetch cache.
   */
  public function _fetchCache() {
    if ($cached = cache_get(CONFIG_SYSTEM_CID, 'cache')) {
      $this->_cache = $cached->data;
    }
    else {
      $lock = 'config_rebuild_cache';
      if (lock_acquire($lock, 10.0)) {
        $this->_rebuildCache();
        lock_release($lock);
      }
      else {
        lock_wait($lock);
        $this->_fetchCache();
      }
    }
  }

  /**
   * Force cache refresh.
   */
  public function _rebuildCache() {
    // FIXME: Find a better algorithm here, it may not scale properly.
    // First fetch known defaults overrides.
    $this->_cache = db_select('config', 'c')
      ->fields('c', array('path', 'value'))
      ->execute()
      ->fetchAllAssoc('path');
    // Edge case, no values are overriden.
    if (!isset($this->_cache)) {
      $this->_cache = array();
    }
    // Read the default configuration and set values.
    foreach (module_invoke_all('config_schema') as $path => $schema) {
      if (!array_key_exists($path, $this->_cache)) {
        $this->_cache[$path] = $schema['default'];
      }
    }
    cache_set(CONFIG_SYSTEM_CID, $this->_cache, 'cache');
  }

  /**
   * Default constructor. Load necessary cache entry.
   */
  public function __construct() {
    if (!isset($this->_cache)) {
      $this->_fetchCache();
    }
  }
}
